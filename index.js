const chalk = require('chalk')

const argv = require('yargs')
		.demandCommand(1)
		.usage(`Usage: ${chalk.yellow('stardew-git')} <${chalk.green('savegamePath')}>`)
		.option('push', {
			alias: 'p',
			description: 'Automatically push to the remote repository',
			default: false,
			type: 'boolean'
		})
		.argv
	, StardewWatcher = require('./lib/stardew-watcher')

new StardewWatcher(argv._[0])
	.init()
	.then(watcher => {
		watcher.run({
			push: argv.push
		})
	})
