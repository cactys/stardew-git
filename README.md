# stardew-git [![NPM version](https://img.shields.io/npm/v/stardew-git.svg)](https://www.npmjs.com/package/stardew-git) [![Pipeline status](https://gitlab.com/metaa/stardew-git/badges/npm/pipeline.svg)](https://gitlab.com/metaa/stardew-git/commits/npm)
> A tool that versions changes in [Stardew Valley][sdv] savegames with git.

## About
This tool allows you to put your [Stardew Valley][sdv] savegames
under version control to back it up and also allow you to go back in
time by checking out earlier commits.

It will create commits with this name pattern: `Day #123.`

## How
[Stardew Valley][sdv] savegames are simple (but huge!) [XML][xml] files,
that are
saved as one-liners (though there are some quest texts that include
linefeeds). This tool simply pretty-prints your savegame and the
`SaveGameInfo` file and commits them to a git repository (which it
creates for you). 

## Requirements
[Git][git] needs to be installed on your system.

... and [Stardew Valley][sdv] of course.

## Usage
### Syntax
```console
stardew-git [-p|--push] <savegamePath>
```

The `--push` flag is optional and allows the tool to automatically push. 

#### Windows
The game was only released for Windows PCs so far, but the syntax would
still be the same; the command, optionss and the path to the savegame's
directory.

```console
stardew-git [-p|--push] %APPDATA%\StardewValley\Saves\<YourSavegame>
```

## Roadmap
-   Add the possibility to use a config file
-   Allow multiple savegames to be watched simultaneously

## Disclaimer
By using this tool you agree to the following:

I can not be held responsible for any kind of damage done to your
savegames. Nothing is perfect and bugs *may* exist.

Please perform a backup of your savegames before using this tool and
report bugs that you may encounter.
 

[git]: https://git-scm.com/
[sdv]: https://stardewvalley.net
[xml]: https://en.wikipedia.org/wiki/XML
